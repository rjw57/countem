import logging
import os

from flask import Flask
import countem

LOG = logging.getLogger(__name__)

app = Flask(__name__)


@app.route('/', methods=['POST'])
def run():
    jobspec_urls = os.environ.get('JOBSPEC_URLS', '').split(',')
    if len(jobspec_urls) == 0:
        LOG.error('No jobspecs defined. Set JOBSPEC_URLS environment variable')
        return {'error': {'message': 'Internal Server Error'}}, 500

    countem.run(countem.load_settings(jobspec_urls))

    return {'status': 'ok'}

# Additional requirements which are only installed inside the Docker container
tox
git+https://gitlab.developers.cam.ac.uk/uis/devops/lib/geddit.git@initial-implementation
flask
gunicorn

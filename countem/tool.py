"""
Generate user stats for the University of Cambridge

Usage:
    countem <job-spec-url>...
    countem (-h | --help)

Options:
    -h, --help  Show a brief usage summary.

    --dry-run   Print what changes would be made but don't make them.

The <job-spec-url> positional arguments should be URLs to a job specification file. See the
"jobspec.yml.example" file in the source distribution for the format. If the URL lacks a scheme,
the "file" scheme is assumed. Supported schemes are "https" and "file".

If multiple job specifications are provided, the specifications are *deep merged*.
"""
import logging
import logging.config

import docopt
import pkg_resources

from . import run, load_settings

logging.config.fileConfig(pkg_resources.resource_filename(__name__, 'logging.conf'))

LOG = logging.getLogger(__name__)


def main():
    opts = docopt.docopt(__doc__)
    settings = load_settings(opts['<job-spec-url>'])
    run(settings)


if __name__ == '__main__':
    main()

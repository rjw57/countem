from ucamlookup import ibisclient
import yaml
import json


def summarise(people):
    return {
        'total': len(people),
        'staff': sum(1 for p in people if p.is_staff()),
        'students': sum(1 for p in people if p.is_student()),
    }


def main():
    with open('secrets.yml') as f:
        secrets = yaml.safe_load(f)

    c = ibisclient.createConnection()
    c.set_username(secrets['lookup']['username'])
    c.set_password(secrets['lookup']['password'])

    pm = ibisclient.PersonMethods(conn=c)
    fetch = []

    def fetch_people():
        identifier = None
        while True:
            people = pm.allPeople(
                includeCancelled=True, identifier=identifier, fetch=fetch, limit=20000
            )
            for p in people:
                yield p
            if len(people) == 0:
                break
            identifier = people[-1].identifier.value

    people = list(fetch_people())
    active_people = [p for p in people if not p.cancelled]

    result = {
        'people': {
            'all': summarise(people),
            'nonCancelled': summarise(active_people),
        },
    }

    print(json.dumps(result, indent=2))


if __name__ == '__main__':
    main()

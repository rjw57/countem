import datetime
import logging
import logging.config
import json

import deepmerge
import geddit
import ucamlookup.ibisclient as ibisclient
import yaml

from .puddit import puddit

LOG = logging.getLogger(__name__)


def run(settings):
    # Create lookup connection
    lookup_settings = settings.get('lookup', {})
    lookup_connection = ibisclient.createConnection()
    if 'username' in lookup_settings:
        lookup_connection.set_username(lookup_settings['username'])
    if 'password' in lookup_settings:
        lookup_connection.set_password(lookup_settings['password'])

    pm = ibisclient.PersonMethods(lookup_connection)
    lookup_queries = lookup_settings.get('queries', [])

    output = {
        'generatedAt': datetime.datetime.now(datetime.timezone.utc).isoformat(),
        'counts': [],
    }
    for q in lookup_queries:
        output['counts'].append({
            'title': q['title'],
            'description': q['description'],
            'query': q['query'],
            'count': pm.searchCount(q['query']),
        })

    puddit(settings['destination'], json.dumps(output), make_public=True)


def load_settings(urls):
    settings = {}
    for url in urls:
        LOG.info('Loading settings from %s', url)
        settings = deepmerge.always_merger.merge(settings, yaml.safe_load(geddit.geddit(url)))
    return settings

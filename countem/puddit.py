import urllib.parse

from google.cloud import storage


def puddit(url, content, *, content_type='application/json', make_public=False):
    components = urllib.parse.urlsplit(url)
    if components.scheme != 'gs':
        raise ValueError(f'Unknown scheme "{components.scheme}" for URL "{url}"')

    bucket = components.netloc
    blob_path = components.path.lstrip('/')
    client = storage.Client()
    bucket = client.get_bucket(bucket)
    blob = bucket.blob(blob_path)
    blob.upload_from_string(content, content_type=content_type)
    if make_public:
        blob.make_public()

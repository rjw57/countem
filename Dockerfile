FROM python:3.6-slim
WORKDIR /tmp/build
RUN apt-get -y update && apt-get -y install git && apt-get clean
ADD ./requirements*.txt ./
RUN pip install --no-cache -r requirements-docker.txt
ADD ./setup.py ./
ADD ./countem ./countem
RUN pip install --no-cache ./ && rm -r /tmp/build
WORKDIR /usr/src/app
ADD ./entrypoint.sh ./app.py /usr/src/app/
ENV PORT=8000
ENTRYPOINT ["./entrypoint.sh"]
